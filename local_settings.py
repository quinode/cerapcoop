# -*- coding:utf-8 -*-

from django.conf import settings
import sys

# Here you can override any settings from coop default settings files
# See :
# - coop/default_project_settings.py
# - coop/db_settings.py

SITE_AUTHOR = 'Cerapcoop'
SITE_TITLE = 'Cerapcoop'
DEFAULT_URI_DOMAIN = 'www.cerapcoop.org'

COOP_USE_SITES = True

# THUMBNAIL_DUMMY = True

# let this setting to False in production, except for urgent debugging
DEBUG = True

# Force DEBUG setting if we're developing locally or testing
if 'runserver' in sys.argv or 'test' in sys.argv:
    DEBUG = True

TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Cerapcoop', 'contact@cerapcoop.org'),
    #('test', 'dom.guardiola@gmail.com'),

)

# PES_HOST = 'http://www.echanges-solidaires-auvergne.fr'
# PES_API_KEY = 'eele5aigohchae3oegh8oophoic5keew2UxuushohRaiquoo2ohxemeezi5Ohpam'

SERVER_EMAIL = 'site@cerapcoop.org'
EMAIL_SUBJECT_PREFIX = '[Site Cerapcoop] '

EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'admin@cerapcoop.org'
EMAIL_HOST_PASSWORD = 'HzQYETGrFI7aCDgMkex5ag'
EMAIL_USE_TLS = True

MANAGERS = ADMINS
SEND_BROKEN_LINK_EMAILS = True
# INTERNAL_IPS = ('127.0.0.1', '92.243.30.98')

# SENTRY_DSN = "http://2537630815df46ac91c1bf1d1a08c0b0:8a2881a53b3c4bb38d97ae9cfed28a56@sentry.quinode.fr/10"

SUBHUB_MAINTENANCE_AUTO = True    # set this value to True to automatically syncronize with agregator
PES_HOST = 'http://pes-auvergne.credis.org'
THESAURUS_HOST = 'thess.domain.com'
URI_FIXED = False

MIDDLEWARE_CLASSES = settings.MIDDLEWARE_CLASSES + ['django.middleware.locale.LocaleMiddleware']
USE_TZ = True

INSTALLED_APPS = settings.INSTALLED_APPS + [
    # select your coop components
    # 'coop.tag',
    'coop.agenda',
    'coop.article',
    'coop.project',
    'coop.mailing',
    'coop.exchange',
    #'coop.webid',
    'coop_local',
    'coop_local.templatetags',

     # coop optional modules
    'coop_geo',  # est obligatoirement APRES coop_local
    # 'geodjangofla',

    # comment on s'en débarasse de ça ?
    'haystack',
    'haystack_fr',

    # mediathèque
    'coop.doc',
    'mptt',
    'media_tree',
    'teambox_icons',
    'easy_thumbnails',
    'datetimewidget',
    
    # Synchro PES
    #'coop_gateway',
]

TINYMCE_DEFAULT_CONFIG = {
    'theme': "advanced",
    'relative_urls': False,
    'width': '617px', 'height': '220px',
    'theme_advanced_toolbar_location': 'top',
    'theme_advanced_buttons1': 'formatselect,bold,italic,|,justifyleft,justifycenter,justifyright,|,bullist,numlist,|,link,unlink,|,code',
    'theme_advanced_buttons2': '', 'theme_advanced_buttons3': '',
    'theme_advanced_blockformats' : 'p,h1,h2,h3,h4,blockquote'
    }


COOP_CMS_ARTICLE_TEMPLATES = [
    ('coop_cms/article.html', 'Standard'),
    ('coop_cms/article_nologo.html', 'Sans logo'),
    ('coop_cms/article_bandeau.html', 'Logo en bandeau'),
]

COOP_CMS_NEWSLETTER_TEMPLATES = [
    ('mailing/newsletter.html', 'Newsletter avec sommaire'),
]

MAILING_BACKEND = 'mandrill'

# TODO: to be discuss this settings could be in default_project_setings.py
# file. To be check I knew more on how to configure sympa
SYMPA_SOAP = {
    'WSDL': 'http://listes.cerapcoop.org/sympa/wsdl',
    'APPNAME': 'quinode_proxy',
    'PASSWORD': 'goKNNc244gSj58wn',
    'OWNER': 'listmaster@cerapcoop.org',
    'PARAMETER_SEPARATOR': '__SEP__',      # used for template
    'SYMPA_TMPL_USER': 'sympaadmin',  # set your sympa User username
    'SYMPA_TMPL_PASSWD': 'adminsympa'  # set your sympa User password
}


ADMINTOOLS_CUSTOM_MENUS = [
    {'title': 'Cerapcoop',
     'icon': 'icon-tint icon-white',
     'children': [
                    ('Annonces', '/admin/coop_local/annonce/'),
                    ('Evenements', '/admin/coop_local/article/?type=event'),
                    ('Financements', '/admin/coop_local/grant/'),
                    ('Ressources documentaires', '/admin/coop_local/docresource/'),
                    ('Mobilisations citoyennes', '/admin/coop_local/article/?category__id__exact=1'),
                    ('Formations', '/admin/coop_local/article/?agenda_category__id__exact=2'),
                    ('Campagnes EAD', '/admin/coop_local/article/?category__id__exact=2'),
                    ('Tags (thématiques)', '/admin/coop_local/tag/'),
                    ('Disciplines', '/admin/coop_local/discipline/'),
                    ('Niveaux d\'enseignement', '/admin/coop_local/niveau/'),
                    ('Catégorie Agenda', '/admin/coop_local/agendacategory/'),

                ]}


    ]
