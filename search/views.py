# -*- coding:utf-8 -*-
from django.conf import settings
from django.shortcuts import render_to_response, redirect
from coop_local.models import (Organization, Article, Exchange, Area,
    Event, Discipline, Grant, Engagement, OrganizationCategory, Project,
    GenericDate, ResourceCategory, DocResource, Niveau, Role, GrantTarget, AgendaCategory)
from django.template import RequestContext
from django.contrib.sites.models import get_current_site
from haystack.views import SearchView
from coop_cms.models import ArticleCategory, Link
from django.shortcuts import render, get_object_or_404
from django.http import Http404, HttpResponse
import json
from django.contrib.sites.models import Site
from coop.views import ModelSearchView as BaseModelSearchView
from django.contrib.sites.models import get_current_site
from django.db.models import Q


def search_resources_form(request):
    formations = AgendaCategory.objects.get(slug='formation')
    campagnes = ArticleCategory.objects.get(name="Campagnes d'éducation au développement")
    mobilisations = ArticleCategory.objects.get(name="Mobilisation citoyenne")
    outils = ResourceCategory.objects.get(slug='outil-methodologique')
    structures = OrganizationCategory.objects.filter(slug='structure-ressource')
    context = {
        #some lists
        'types_formation': [
            {'label': 'Formations CERAPCOOP', 'slug':'f_cerapcoop'},
            {'label': 'Formations en Auvergne', 'slug':'f_auvergne'},
            {'label': 'Autres formations', 'slug':'f_ailleurs'},
            ],
        'types_doc': ResourceCategory.objects.exclude(label=outils),
        'niveaux': Niveau.objects.all(),
        'publics': Role.objects.filter(cible_base_peda=True),
        'cibles': GrantTarget.objects.all(),
        'zones': Area.objects.filter(area_type_id__exact=5).order_by('label'),
        # last published
        'financements': Grant.objects.all()[:4],
        'formations': Article.objects.filter(agenda_category=formations)[:4],
        'documents': DocResource.objects.exclude(category=outils)[:4],
        'outils': DocResource.objects.filter(category=outils)[:4],
        'campagnes': Article.objects.filter(category=campagnes)[:4],
        'mobilisations': Article.objects.filter(category=mobilisations)[:4],
        'structures': Organization.objects.filter(category=structures),
    }
    qs = Link.objects.filter(url=request.path)
    if qs.exists():
        context['link'] = qs[0]
    return render_to_response('ressources/liste.html', context, RequestContext(request))


def grab_documents(**kwargs):
    outils = ResourceCategory.objects.get(slug='outil-methodologique')
    results = []
    res = DocResource.objects.exclude(category=outils)
    if 'q' in kwargs.keys() and kwargs['q'] != None:
        query = kwargs['q']
        res= res.filter(
            Q(label__icontains=query) | Q(slug__icontains=query) | Q(description__icontains=query) 
                | Q(organization__title__icontains=query)
            )
    if 'zone' in kwargs.keys():
        res = res.filter(zone=Area.objects.get(id=kwargs['zone']))
    if 'type_doc' in kwargs.keys():
        res = res.filter(category=ResourceCategory.objects.get(id=kwargs['type_doc']))
    if 'niveau' in kwargs.keys():
        res = res.filter(niveaux__in=[Niveau.objects.get(id=kwargs['niveau'])])
    if 'public' in kwargs.keys():
        res = res.filter(cible__in=[Role.objects.get(id=kwargs['public'])])
    for r in res:
        results.append({'label': unicode(r.label), 'url':r.get_absolute_url(), 
                        'type': u'Document', 'class': 'res_doc'})
    return results


def grab_outils(**kwargs):
    outils = ResourceCategory.objects.get(slug='outil-methodologique')
    results = []
    res = DocResource.objects.filter(category=outils)
    if 'q' in kwargs.keys() and kwargs['q'] != None:
        query = kwargs['q']
        res= res.filter(
            Q(label__icontains=query) | Q(slug__icontains=query) | Q(description__icontains=query) 
                | Q(organization__title__icontains=query)
            )
    if 'zone' in kwargs.keys():
        res = res.filter(zone=Area.objects.get(id=kwargs['zone']))
    if 'type_doc' in kwargs.keys():
        res = res.filter(category=ResourceCategory.objects.get(id=kwargs['type_doc']))
    if 'niveau' in kwargs.keys():
        res = res.filter(niveaux__in=[Niveau.objects.get(id=kwargs['niveau'])])
    if 'public' in kwargs.keys():
        res = res.filter(cible__in=[Role.objects.get(id=kwargs['public'])])
    for r in res:
        results.append({'label': unicode(r.label), 'url':r.get_absolute_url(), 
                        'type': u'Outil méthodo.', 'class': 'res_outil'})
    return results


def grab_campagnes(**kwargs):
    campagnes = ArticleCategory.objects.get(name="Campagnes d'éducation au développement")
    results = []
    res = Article.objects.filter(category=campagnes)
    if 'q' in kwargs.keys() and kwargs['q'] != None:
        query = kwargs['q']
        res= res.filter(
            Q(title__icontains=query) | Q(slug__icontains=query) | Q(content__icontains=query) 
                | Q(summary__icontains=query)| Q(organization__title__icontains=query)
            )
    if 'zone' in kwargs.keys():
        res = res.filter(zone=Area.objects.get(id=kwargs['zone']))    
    for r in res:
        results.append({'label': unicode(r.title), 'url':r.get_absolute_url(), 
                        'type': u'Campagne', 'class': 'res_camp'})
    return results

def grab_mobs(**kwargs):
    mobilisations = ArticleCategory.objects.get(name="Mobilisation citoyenne")
    results = []
    res = Article.objects.filter(category=mobilisations)
    if 'q' in kwargs.keys() and kwargs['q'] != None:
        query = kwargs['q']
        res= res.filter(
            Q(title__icontains=query) | Q(slug__icontains=query) | Q(content__icontains=query) 
                | Q(summary__icontains=query)| Q(organization__title__icontains=query)
            )
    if 'zone' in kwargs.keys():
        res = res.filter(zone=Area.objects.get(id=kwargs['zone']))    
    for r in res:
        results.append({'label': unicode(r.title), 'url':r.get_absolute_url(),
                        'type': u'Mobilisation', 'class': 'res_mob'})
    return results

def grab_grants(**kwargs):
    results = []
    if 'q' in kwargs.keys() and kwargs['q'] != None:
        query = kwargs['q']
        res = Grant.objects.filter(
                Q(label__icontains=query) | Q(slug__icontains=query) | Q(description__icontains=query) 
                 | Q(summary__icontains=query)| Q(organization__title__icontains=query)
            )
    else:
        res = Grant.objects.all() 
    if 'cible' in kwargs.keys():
        res = res.filter(target=GrantTarget.objects.get(id=kwargs['cible']))
    for r in res:
        results.append({'label': unicode(r.label), 'url':r.get_absolute_url(),
                        'type': u'Financement', 'class': 'res_grant'})
    return results

def grab_formations(**kwargs):
    # print kwargs
    results = []
    formations = AgendaCategory.objects.get(slug='formation')
    res = Article.objects.filter(agenda_category=formations)
    # filtre mot-clé
    if 'q' in kwargs.keys() and kwargs['q'] != None:
        query = kwargs['q']
        res = res.filter(
                Q(title__icontains=query) | Q(slug__icontains=query) | Q(content__icontains=query) 
                | Q(summary__icontains=query) | Q(organization__title__icontains=query))
    # autres filtres
    if 'type_formation' in kwargs.keys():
        from coop_local.models import ZONAGE_AGENDA
        if kwargs['type_formation'] == 'f_cerapcoop':
            res = res.filter(organization=Organization.objects.get(title=u'CERAPCOOP'))
        else:
            res = res.exclude(organization=Organization.objects.get(title=u'CERAPCOOP'))
            if kwargs['type_formation'] == 'f_auvergne':

                res = res.filter(agenda_zone__in = [ZONAGE_AGENDA.D03, ZONAGE_AGENDA.D63, 
                    ZONAGE_AGENDA.D43, ZONAGE_AGENDA.D15])

            elif kwargs['type_formation'] == 'f_ailleurs':
                res = res.filter(agenda_zone__in = [ZONAGE_AGENDA.FRA, ZONAGE_AGENDA.EUR, ZONAGE_AGENDA.INT])
    for r in res:
        results.append({'label': unicode(r.title), 'url':r.get_absolute_url(),
                        'type': u'Formation', 'class': 'res_form'})
    # print results
    return results



def kwargs_if_present(params, filter_list):
    kwargs = {'q':params.get('q', None)}
    for f in filter_list:
        if f in params.keys():
            kwargs[f] = params[f]
    return kwargs


def search_resources_results(request):
    # from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
    context = {}
    results = []
    params = {}
    nb = 0
    for p in request.GET.items():
        if p[1] != u'':
            params[p[0]] = p[1]

    # dict de correspondance filtre--->fonction
    fmap = {
        'financements' : {'filters': ['cible'], 'func': grab_grants},
        'formations': {'filters': ['type_formation'], 'func': grab_formations},
        'documents': {'filters': ['type_doc', 'public', 'niveau', 'zone'], 'func': grab_documents},
        'outils': {'filters': ['type_doc', 'public', 'niveau', 'zone'], 'func': grab_outils},
        'campagnes': {'filters':['zone'], 'func': grab_campagnes},
        'mobilisations': {'filters':['zone'], 'func': grab_mobs },
    }

    #on lance la ou les fonctions de recherche
    if 'res_type' in params.keys() and params['res_type'] in fmap:
        # recherche sur un seul modèle
        results = fmap[params['res_type']]['func'](**kwargs_if_present(params, fmap[params['res_type']]['filters']))
        nb = len(results)
    else:
        #recherche sur tous les modèles
        for model in fmap.items():
            # import pdb; pdb.set_trace()
            results += fmap[model[0]]['func'](**kwargs_if_present(params, fmap[model[0]]['filters']))
            nb += len(results)
    # on remet les critères dans le contexte (pourquoi ??? on est en json...)
    if 'q' in params.keys():
        # print params
        context['query'] = params['q']
    context['results'] = results
    context['nb'] = nb

    # paginator = Paginator(results_list, 20) # Show 25 contacts per page
    # page = request.GET.get('page')
    # try:
    #     results = paginator.page(page)
    # except PageNotAnInteger:
    #     # If page is not an integer, deliver first page.
    #     results = paginator.page(1)
    # except EmptyPage:
    #     # If page is out of range (e.g. 9999), deliver last page of results.
    #     results = paginator.page(paginator.num_pages)
    return render_to_response('search/ressources.json', context, RequestContext(request), mimetype="application/json")



