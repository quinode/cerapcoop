# -*- coding:utf-8 -*-
from django.contrib import admin
from django.conf.urls.defaults import patterns, include, url
from haystack.views import search_view_factory
from haystack.forms import SearchForm
from coop_local.views import ModelSearchView, SiteModelSearchView
from coop.forms import SiteSearchForm
from django.contrib.sites.models import Site

from coop_local.models import GenericDate
# # https://code.djangoproject.com/ticket/10405#comment:11
# from django.db.models.loading import cache as model_cache
# if not model_cache.loaded:
#     model_cache.get_models()

admin.autodiscover()

# Add you own URLs here
urlpatterns = []

urlpatterns += patterns('',
    url(r'^$', 'coop_local.views.home', name="home"),
    url(r'^inscription-mailing/$', 'coop_local.views.inscription_mailing', name="inscription_mailing"),
    url(r'^ressources/$', 'coop_local.search.views.search_resources_form', name="ressources"),
    url(r'^rubrique/ressources/$', 'coop_local.search.views.search_resources_form', name="ressources"),
    url(r'^search/ressources/$', 'coop_local.search.views.search_resources_results', name="ressources_results"),

    url(r'^financements/$', 'coop_local.views.grants_list', name="grants_list"),
    url(r'^financement/(?P<slug>[\w-]+)/$', 'coop_local.views.grant_detail', name="grant_detail"),

    url(r'^annonces/$', 'coop_local.views.annonces_list', name="annonces_list"),
    url(r'^annonces/(?P<slug>[\w-]+)/$', 'coop_local.views.annonces_category', name="annonces_category"),
    url(r'^annonce/(?P<slug>[\w-]+)/$', 'coop_local.views.annonce_detail', name="annonce_detail"),

    url(r'^contacts/$', 'coop_local.views.contacts', name="contacts"),
    url(r'^agenda/?$', 'coop_local.views.agenda_global', {}, name="agenda_liste"),
    url(r'^agenda/auvergne/?$', 'coop_local.views.agenda_global', {'filter':'Auvergne'}, name="agenda_auvergne"),
    url(r'^agenda/cerapcoop/?$', 'coop_local.views.agenda_global', {'filter':'Cerapcoop'}, name="agenda_cerapcoop"),
    url(r'^agenda/proposer/$', 'coop_local.views.add_event', {}, name="add_event"),
    url(r'^formations/?$', 'coop_local.views.agenda_global', {'filter': 'Formation'}, name="agenda_formation"),
    url(r'^plan-du-site/$', 'coop_local.views.plan', name="plan-du-site"),
    url(r'^tag/(?P<slug>[\w-]+)/((?P<param>.+)/)?$', 'coop_local.views.tag_detail', name="tag_detail"),

    # url(r'^agenda/?$', 'coop.agenda.views.month_view', {'queryset': GenericDate.objects.all(), 'articles':True }, name='agenda-default'),

    # url(r'^discipline/(?P<slug>[\w-]+)/$', 'coop_local.views.discipline_detail', name="discipline_detail"),

#    	url(r'^org/$', 'coop_local.views.org_list', name="org_list"), #view coop
)

urlpatterns += patterns('haystack.views',
        url(r'^search/search/$', search_view_factory(
            view_class=SiteModelSearchView,
            form_class=SiteSearchForm,
            site=Site.objects.get_current(),
            template='search/search.html',
            load_all=False
        ), name='haystack_search'),
    )

urlpatterns += patterns('haystack.views',
        url(r'^search/searchcerapcoop/$', search_view_factory(
            view_class=SiteModelSearchView,
            form_class=SiteSearchForm,
            site=Site.objects.get(id=1),
            template='search/search.html',
            load_all=False
        ), name='haystack_search'),
    )

urlpatterns += patterns('haystack.views',
        url(r'^search/searchall/$', search_view_factory(
            view_class=ModelSearchView,
            form_class=SearchForm,
            template='search/search.html',
            load_all=False
        ), name='haystack_search'),
)

from coop.default_project_urls import urlpatterns as default_project_urls
urlpatterns += default_project_urls

