jQuery(function($){
    function doBlink( obj, count ) {
        for( var i = 0 ; i < count ; i++ )
        {
            obj.fadeOut(300).fadeIn(300);
        }
    }

    doBlink($('nav .dropdown a'), 2);

    $('.dropdown').on('click', function(){
        $(this).find('.dropdown-menu').toggle(300);
    });
});
