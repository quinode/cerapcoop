# -*- coding:utf-8 -*-
from django.db import models
from django.db.models import Q
from extended_choices import Choices
from django.contrib.sites.models import Site
from coop.org.models import BaseOrganization, BaseEngagement, BaseRole
from coop.agenda.models import BaseGenericDate
from coop.mailing.models import BaseNewsletter
from coop.doc.models import BaseDocResource
from coop.exchange.models import BaseExchange
from coop.project.models import BaseProject
from coop.person.models import BasePerson
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.urlresolvers import reverse
from django_extensions.db import fields as exfields
from sorl.thumbnail import ImageField
from sorl.thumbnail import default
from coop.prefs.models import BaseSitePrefs
from coop.models import URIModel
from coop.article.models import CoopArticle, CoopNavTree
import datetime
from coop_cms.models import ArticleCategory
from django.db.models.loading import get_model
from django.core.exceptions import ImproperlyConfigured, ObjectDoesNotExist
import rdflib


class SitePrefs(BaseSitePrefs):
    eadsi_encart_1 = models.ForeignKey('coop_local.Article', null=True, blank=True,
                                 related_name='encart1_link',
                                 verbose_name=u'Colonne : actualité n°1')
    eadsi_encart_2 = models.ForeignKey('coop_local.Article', null=True, blank=True,
                                 related_name='encart2_link',
                                 verbose_name=u'Colonne : actualité n°2')
    eadsi_encart_3 = models.ForeignKey('coop_local.Article', null=True, blank=True,
                                 related_name='encart3_link',
                                 verbose_name=u'Colonne : actualité n°3')
    cerapcoop_tags = models.ManyToManyField('coop_local.Tag', verbose_name=u'Thémes colonne de droite', blank=True, null=True)
    cerapcoop_intitule_1 = models.CharField(u'Intitulé encadré bleu', max_length=50, blank=True, null=True)
    cerapcoop_intitule_2 = models.CharField(u'Intitulé encadré orange', max_length=50, blank=True, null=True)
    cerapcoop_thematique = models.ForeignKey('coop_local.Tag', null=True, blank=True,
                                 related_name='thematique',
                                 verbose_name=u'Thématique')
    cerapcoop_initiative = models.ForeignKey('coop_local.Organization', null=True, blank=True,
                                 related_name='initiative',
                                 verbose_name=u'Initiative')

    ml_eadsi = models.ForeignKey('coop_local.MailingList', null=True, blank=True,
                                 related_name='ml_eadsi',
                                 verbose_name=u'Mailing liste générale')

    ml_cerapcoop = models.ForeignKey('coop_local.MailingList', null=True, blank=True,
                                 related_name='ml_cerapcoop',
                                 verbose_name=u'Mailing liste générale')

    cerapcoop_article_1 = models.ForeignKey('coop_local.Article', null=True, blank=True,
                                 related_name='cerapcoop_article_1',
                                 verbose_name=u'Accueil : Article n°1')

    cerapcoop_article_2 = models.ForeignKey('coop_local.Article', null=True, blank=True,
                                 related_name='cerapcoop_article_2',
                                 verbose_name=u'Accueil : Article n°2')

    def eadsi_encarts(self):
        return [self.eadsi_encart_1, self.eadsi_encart_2, self.eadsi_encart_3]

class NavTree(CoopNavTree):
    pass

class DefaultMAJ(object):
    def save(self, *args, **kwargs):
        if not self.maj:
            self.maj = datetime.datetime.now()
        super(DefaultMAJ, self).save(*args, **kwargs)


STATUTS = Choices(
    ('ASSO', 1,  u'Association Loi 1901'),
    ('JUM',  2,  u'Comité de jumelage'),
    ('ONG',  3,  u'Organisation internationale non gouvernementale'),
    ('COLL', 4,  u'Collectivité locale'),
    ('EPCI', 5,  u'Etablissement public de coopération intercommunal (EPCI)'),
    ('ETAT', 6,  u'Institution d’Etat'),
    ('PARA', 7,  u'Etablissement ou service parapublic'),
    ('HOP',  8,  u'Clinique, Hôpital'),
    ('PAYS', 9,  u'Pays'),
    ('EPLE', 10, u'Etablissement scolaire (lycée, collège, école)'),
    ('UNIV', 11, u'Université ou Institut ou Grande école'),
    ('SOC',  12, u'Organisme socioprofessionnel'),
    ('CONS', 13, u'Organisme consulaire'),
    ('PRIV', 14, u'Société privée'),
    ('CE',   15, u'Comité d’entreprise'),
    ('SYND', 16, u'Syndicat d’entreprise'),
    ('FOND', 17, u'Fondation'),
    ('AUT',  18, u'Autre'),
    ('REG',  19, u'Dispositif régional'),
    ('MIXT', 20, u'Syndicat mixte'),
)

DIMENSION = Choices(
    ('LOCAL',   1,  u'Locale'),
    ('DEPT',    2,  u'Départementale'),
    ('REGION',  3,  u'Régionale'),
    ('NATIONAL', 4,  u'Nationale'),
    ('INTER',   5,  u'Internationale')
)

ZONAGE_AGENDA = Choices(
    ('D03', 1, u'Allier'),
    ('D15', 2, u'Cantal'),
    ('D43', 3, u'Haute-Loire'),
    ('D63', 4, u'Puy-de-Dôme'),
    ('FRA', 5, u'France (hors Région)'),
    ('EUR', 6, u'Europe'),
    ('INT', 7, u'International'),
)

EXCHANGE_DISPO = Choices(
    ('LOCAL',   1,  u'Locale'),
    ('NATIONAL', 2,  u'Nationale'),
    ('EURO',   3,  u'Européenne')
)

BUDGET = (
    (1,   u"moins de 1 500 €"),
    (2,   u"de 1 500 € à 7 600 €"),
    (3,   u"de 7 600 € à 38 000 €"),
    (4,   u"de 38 000 € à 150 000 €"),
    (5,   u"de 150 000 € à moins de 450 000 €"),
    (6,   u"de 450 000 € à moins de 2 M€"),
    (7,   u"de 2 M€ à moins de 7,6 M€"),
    (8,   u"7,6 M€ ou plus"),
)

CIVILITES = (
    (1, u"Madame"),
    (2, u"Monsieur"),
    (3, u"Docteur"),
    (4, u"Monseigneur"),
)

class Organization(DefaultMAJ, BaseOrganization):
    code_gestion = models.CharField(u'Code Gestion', blank=True, null=True, max_length=50)
    accueil = models.CharField(u"Horaires d'ouverture", blank=True, null=True, max_length=200)
    outils_peda = models.CharField(u"Outils pédagogiques", blank=True, null=True, max_length=200)
    part_pvd = models.PositiveSmallIntegerField(u"% du budget consacré aux PVD", blank=True, null=True)
    budget = models.PositiveSmallIntegerField(u'Budget annuel',
                                              choices=BUDGET,
                                              default=1)
    salaries = models.PositiveIntegerField(u"Nb de salariés", blank=True, null=True)
    adherents = models.PositiveIntegerField(u"Nb d'adhérents'", blank=True, null=True)

    dimension = models.PositiveSmallIntegerField(u'Dimension',
                                                 choices=DIMENSION.CHOICES,
                                                 default=DIMENSION.LOCAL)
    statut = models.PositiveSmallIntegerField(u'Statut juridique',
                                              choices=STATUTS.CHOICES,
                                              default=STATUTS.ASSO)
    maj = models.DateField(u'Mise à jour', blank=True, null=True)

    def title(self):
        return unicode(self.label())

    class Meta:
        verbose_name = _(u"structure")
        verbose_name_plural = _(u"structures")
        app_label = 'coop_local'
        ordering = ['-maj']


class Person(BasePerson):
    civilite = models.PositiveSmallIntegerField(u"Civilité", blank=True, null=True, choices=CIVILITES)



class Article(DefaultMAJ, CoopArticle):
    maj = models.DateField(u'Mise à jour', blank=True, null=True)
    zone = models.ForeignKey('coop_local.Area', verbose_name="Pays (si hors-France)", null=True, blank=True)
    agenda_category = models.ForeignKey('coop_local.AgendaCategory', verbose_name=u"Type d'évènement", null=True, blank=True)
    agenda_zone = models.PositiveSmallIntegerField(u'Localisation',
                                                 choices=ZONAGE_AGENDA.CHOICES,
                                                 # default=ZONAGE_AGENDA.D63,
                                                 null=True, blank=True)
    agenda_commune = models.ForeignKey('coop_local.Area', verbose_name=u'Commune (France)',
        related_name='agenda_commune', blank=True, null=True)
    agenda_public = models.ManyToManyField('coop_local.GrantTarget', 
                                        verbose_name=u'Public cible', null=True, blank=True, related_name='agenda_public')

    class Meta:
        verbose_name = _(u"article")
        verbose_name_plural = _(u"articles")
        app_label = 'coop_local'
        ordering = ['-maj']

    def is_event(self):
        return self.agenda_category != None
        
    is_event.boolean = True
    is_event.short_description = _(u'Evènement')

    def save(self, *args, **kwargs):
        try:
            Fra = get_model('coop_local','Area').objects.get(label='France')
            Eur = get_model('coop_local','Area').objects.get(label='Europe')
        except ObjectDoesNotExist, exp:
            raise ImproperlyConfigured('Les zones "France" et "Europe" ne sont pas dans la base géographique. %s' % exp)
        if self.agenda_commune : # on suppose qu'on n'a pas de commune d'autres pays...
            if self.agenda_commune.reference.startswith('63'):
                self.agenda_zone = ZONAGE_AGENDA.D63
            elif self.agenda_commune.reference.startswith('43'):
                self.agenda_zone = ZONAGE_AGENDA.D43
            elif self.agenda_commune.reference.startswith('15'):
                self.agenda_zone = ZONAGE_AGENDA.D15
            elif self.agenda_commune.reference.startswith('03'):
                self.agenda_zone = ZONAGE_AGENDA.D03
            elif self.agenda_commune and self.agenda_commune.parent.parent.parent == Fra : # dep-reg-pays
                self.agenda_zone = ZONAGE_AGENDA.FRA
        elif self.zone:
            if self.zone.parent == Eur:
                self.agenda_zone = ZONAGE_AGENDA.EUR
            elif self.zone:
                self.agenda_zone = ZONAGE_AGENDA.INT
        else:
            self.agenda_zone = None
        super(Article, self).save(*args, **kwargs)


class Engagement(BaseEngagement):
    est_contact = models.BooleanField(u"Contact", default=True)


# class GenericDate(BaseGenericDate):
#     commune_fr = models.ForeignKey('coop_local.Area', verbose_name=u'Commune (France)',
#         related_name='commune', blank=True, null=True)
#     pays = models.ForeignKey('coop_local.Area', 
#         related_name='pays', verbose_name=u'Pays (hors France)', blank=True, null=True)


# class LogEventType(models.Model):
#     name = models.CharField('nom', max_length=50)
#     text_field_label = models.CharField('libellé du champ "texte"', max_length=50)

#     has_year_field = models.BooleanField('utilise le champ "année"', default=False)
#     year_field_label = models.CharField('libellé du champ "année"', max_length=50, blank=True)

#     has_date_field = models.BooleanField('utilise le champ "date"', default=False)
#     date_field_label = models.CharField('libellé du champ "date"', max_length=50, blank=True)

#     class Meta:
#         verbose_name = u"Type d'événement de l'historique"
#         verbose_name_plural = u"Type d'événement de l'historique"
#         app_label = 'coop_local'


# class Log(models.Model):
#     category = models.ForeignKey(LogEventType)
#     year_field = models.DateField('année')
#     date_field = models.DateField('date')
#     text_field = models.TextField('texte')
#     event = models.ForeignKey('coop_local.Event', null=True, blank=True)
#     exchange = models.ForeignKey('coop_local.Exchange', null=True, blank=True, related_name='related_exchange')
#     author = models.ForeignKey('coop_local.Person', null=True, blank=True, related_name='action_author')

#     class Meta:
#         verbose_name = u'Historique'
#         app_label = 'coop_local'


# class Logged(models.Model):
#     # things which have a log
#     log = models.ForeignKey('coop_local.Log', null=True, blank=True,
#                             verbose_name=_(u"log"))
#     # generic key to other objects
#     content_type = models.ForeignKey(ContentType, blank=True, null=True, default=None)
#     object_id = models.PositiveIntegerField()
#     content_object = generic.GenericForeignKey('content_type', 'object_id')

#     def __unicode__(self):
#         return u"Log for" + unicode(self.content_object) + u" : " + unicode(self.log.category) + " - " + unicode(self.log.category.text_field)

#     class Meta:
#         verbose_name = _(u'Logged item')
#         verbose_name_plural = _(u'Logged items')
#         app_label = 'coop_local'


class AgendaCategory(models.Model):
    label = models.CharField(u"intitulé", max_length=50)
    slug = exfields.AutoSlugField(populate_from='label', blank=True, overwrite=True)

    class Meta:
        verbose_name = "catégorie agenda"
        verbose_name_plural = "catégories agenda"
        app_label = 'coop_local'

    def __unicode__(self):
        return unicode(self.label)



class Discipline(models.Model):
    label = models.CharField(u"intitulé", max_length=250)
    slug = exfields.AutoSlugField(populate_from='label', blank=True, overwrite=True)

    def __unicode__(self):
        return self.label

    def get_absolute_url(self):
        return reverse('discipline_detail', args=[self.slug])

    class Meta:
        app_label = 'coop_local'
        ordering = ['label']


class Niveau(models.Model):
    label = models.CharField(u"intitulé", max_length=250)

    def __unicode__(self):
        return self.label

    class Meta:
        verbose_name = u"Niveau d'enseignement"
        verbose_name_plural = "Niveaux d'enseignemement"
        app_label = 'coop_local'


# Personnalisation de modèles relatifs à la base de ressources


class Role(BaseRole):
    cible_base_peda = models.BooleanField('Public cible', default=False)


# class Exchange(BaseExchange):
#     # Niveaux M2M ?
#     cible = models.ManyToManyField('coop_local.Role', verbose_name=u'Public cible')
#     dispo = models.PositiveSmallIntegerField(u'Disponibilité',
#                                              choices=EXCHANGE_DISPO.CHOICES,
#                                              default=EXCHANGE_DISPO.LOCAL)


class DocResource(DefaultMAJ, BaseDocResource):
    cible = models.ManyToManyField('coop_local.Role', verbose_name=u'Public cible')
    niveaux = models.ManyToManyField(Niveau, verbose_name=u"Niveaux d'enseignement")
    maj = models.DateField(u'Mise à jour', blank=True, null=True)
    # zone = generic.GenericRelation('coop_geo.AreaLink')  # , related_name='framed_org')

    def title(self):
        return unicode(self.label)

    class Meta:
        verbose_name = _(u"ressource documentaire")
        verbose_name_plural = _(u"ressources documentaires")
        app_label = 'coop_local'
        ordering = ['maj']
    # Niveaux 


class Exchange(DefaultMAJ, BaseExchange):
    maj = models.DateField(u'Mise à jour', blank=True, null=True)


class Project(DefaultMAJ, BaseProject):
# spécifique EAD
    ead_objectifs = models.TextField(u'Objectifs pédagogiques')
    ead_disciplines = models.ManyToManyField(Discipline, verbose_name=u"Disciplines associées")
    ead_niveaux = models.ManyToManyField(Niveau, verbose_name=u"Niveaux d'enseignement")
    ead_nb_eleves = models.PositiveSmallIntegerField(u"Nombre d'élèves impliqués")
    ead_nb_profs = models.PositiveSmallIntegerField(u"Nombre d'enseignant(e)s impliqués")
    # Niveaux 
    cible = models.ManyToManyField('coop_local.Role', verbose_name=u'Public (outils pédagogiques)')
    maj = models.DateField(u'Mise à jour', blank=True, null=True)

    class Meta:
        verbose_name = _(u"projet")
        verbose_name_plural = _(u"projets")
        app_label = 'coop_local'
        ordering = ['-maj']


class Annonce(DefaultMAJ, URIModel):
    label = models.CharField(u"intitulé", max_length=50)
    slug = exfields.AutoSlugField(populate_from='label', blank=True, overwrite=True)
    description = models.TextField(u"Description")
    annonce_category = models.ForeignKey('coop_local.AnnonceCategory', verbose_name=u"Catégorie d'annonce", blank=True, null=True)
    web = models.URLField(u"Lien web", blank=True, null=True)
    organization = models.ForeignKey('coop_local.Organization', verbose_name=u"Organisme référent", null=True, blank=True)
    person = models.ForeignKey('coop_local.Person', null=True, blank=True, verbose_name=u"Personne contact")
    maj = models.DateField(u'Mise à jour', blank=True, null=True)
    expiration = models.DateField(u'Date d’expiration', blank=True, null=True)

    tel = models.CharField(u"téléphone", max_length=50, blank=True, null=True,
        help_text=u"ce numéro sera affiché sur l'annonce")
    email = models.CharField(u"email", max_length=50, blank=True, null=True, 
                help_text=u'votre adresse mail ne sera pas affichée')

    remote_person_uri = models.URLField(_('remote person URI'), blank=True, max_length=255)
    remote_person_label = models.CharField('Nom du contact',
                                           max_length=250, blank=True, null=True)
    remote_organization_uri = models.URLField(_('remote organization URI'), blank=True, max_length=255)
    remote_organization_label = models.CharField('Structure',
                                                 max_length=250, blank=True, null=True)
    newsletter = models.ForeignKey('coop_local.Newsletter', verbose_name=u'newsletter',
                                blank=True, null=True, related_name='news_annonces',
                                on_delete=models.SET_NULL)
    class Meta:
        ordering = ['-maj']
        verbose_name = "annonce"
        verbose_name_plural = "annonces"
        app_label = 'coop_local'

    def __unicode__(self):
        return unicode(self.label)

    def save(self, *args, **kwargs):
        if not self.pk and self.active:
            self.active = False
            self.save()
            from django.core import urlresolvers
            change_url = urlresolvers.reverse('admin:coop_local_annonce_change', args=(self.id,))
            from django.core.mail import mail_admins
            email_text = u'''Une nouvelle annonce a été postée sur le site : 
Sujet : "''' + unicode(self.label) + u'''" 
Texte : 
    ''' + unicode(self.description) + u'''
                
Pour la valider, suivez le lien suivant : 
http://'''+ settings.DEFAULT_URI_DOMAIN + change_url 
            mail_admins(u'Nouvelle annonce sur le site', email_text , fail_silently=False)
            
        super(Annonce, self).save(*args, **kwargs)

    def is_active(self):
        return self.active
        
    is_active.boolean = True
    is_active.short_description = _(u'En ligne')

    def get_absolute_url(self):
        return reverse('annonce_detail', args=[self.slug])

    def organization_display(self):
        if not self.organization:
            if not self.remote_organization_label:
                return None
            else:
                return self.remote_organization_label
        else:
            return self.organization.label()

    organization_display.short_description = "Organisme"

    rdf_type = settings.NS.dct.Text

class AnnonceCategory(models.Model):
    label = models.CharField(u"intitulé", max_length=50)
    slug = exfields.AutoSlugField(populate_from='label', blank=True, overwrite=True)

    class Meta:
        verbose_name = "catégorie d'annonce"
        verbose_name_plural = "catégories d'annonce"
        app_label = 'coop_local'

    def __unicode__(self):
        return unicode(self.label)

GRANTS_TYPE = (
    (1, "Public / Régional"),
    (2, "Public / National"),
    (3, "Public / Europééns"),
    (4, "Public / International"),
    (5, "Privé / Fondation"),
    (6, "Privé / Autres"),
)

class GrantTarget(models.Model):
    label = models.CharField(u"intitulé", max_length=50)
    slug = exfields.AutoSlugField(populate_from='label', blank=True, overwrite=True)

    def __unicode__(self):
        return self.label

    class Meta:
        verbose_name=u'Cible'
        verbose_name_plural = u'Cibles'
        app_label = 'coop_local'
        ordering = ['label']



class Grant(DefaultMAJ, URIModel):
    label = models.CharField(u"intitulé", max_length=250)
    slug = exfields.AutoSlugField(populate_from='label', blank=True, overwrite=True)
    summary = models.TextField(u"Résumé")
    description = models.TextField(u"Description")
    logo = ImageField(upload_to='logos/', null=True, blank=True)
    web = models.URLField(u"Lien web référence", blank=True, null=True)
    organization = models.ForeignKey('coop_local.Organization', verbose_name=u"Organisme référent", null=True, blank=True)
    person = models.ForeignKey('coop_local.Person', null=True, blank=True, verbose_name=u"Personne contact")
    documents = models.ManyToManyField('coop_cms.Document', verbose_name=u"Fichiers attachés")

    grant_type = models.PositiveSmallIntegerField(u"Type", choices=GRANTS_TYPE)
    target = models.ManyToManyField("coop_local.GrantTarget", verbose_name=u"Cible")
    expiration_date = models.DateField(u"Date d'expiration", null=True, blank=True)

    zone = generic.GenericRelation('coop_geo.AreaLink')  # , related_name='framed_org')

    remote_person_uri = models.URLField(_('remote person URI'), blank=True, max_length=255)
    remote_person_label = models.CharField(_(u'remote person label'),
                                           max_length=250, blank=True, null=True,
                                           help_text=_(u'fill this only if the person record is not available locally'))
    remote_organization_uri = models.URLField(_('remote organization URI'), blank=True, max_length=255)
    remote_organization_label = models.CharField(_(u'remote organization label'),
                                                 max_length=250, blank=True, null=True,
                                                 help_text=_(u'fill this only if the organization record is not available locally'))

    attachments = generic.GenericRelation('coop_local.Attachment')
    external_links = generic.GenericRelation('coop_local.Link')
    maj = models.DateField(u'Mise à jour', blank=True, null=True)

    newsletter = models.ForeignKey('coop_local.Newsletter', verbose_name=u'newsletter',
                                blank=True, null=True, related_name='news_grant',
                                on_delete=models.SET_NULL)

    class Meta:
        ordering = ['-maj']
        verbose_name = "financement"
        verbose_name_plural = "financements"
        app_label = 'coop_local'

    def __unicode__(self):
        return unicode(self.label)

    def title(self):
        return unicode(self.label)

    def get_absolute_url(self):
        return reverse('grant_detail', args=[self.slug])

    def in_zone(self, zone_id):
        for z in self.zone.all():
            if z.location.parent:
                if z.location.parent.id == zone_id or z.location.id == zone_id:
                    return True
        return False

    def organization_display(self):
        if not self.organization:
            if not self.remote_organization_label:
                return None
            else:
                return self.remote_organization_label
        else:
            return self.organization.label()

    organization_display.short_description = "Organisme"

    def logo_list_display(self):
        try:
            if self.logo:
                thumb = default.backend.get_thumbnail(self.logo.file, settings.ADMIN_THUMBS_SIZE)
                return '<img width="%s" src="%s" />' % (thumb.width, thumb.url)
            else:
                return _(u"No Image")
        except IOError:
            return _(u"No Image")

    logo_list_display.short_description = _(u"logo")
    logo_list_display.allow_tags = True

    rdf_type = settings.NS.dcmi.Text
    base_mapping = [
            ('single_mapping', (settings.NS.dct.created, 'created'), 'single_reverse'),
            ('single_mapping', (settings.NS.dct.modified, 'modified'), 'single_reverse'),
            ('single_mapping', (settings.NS.dct.title, 'label'), 'single_reverse'),
            ('single_mapping', (rdflib.URIRef(str(settings.NS['dct']) + 'abstract'), 'summary'), 'single_reverse'),  # bug in rdflib
            ('single_mapping', (settings.NS.dct.description, 'description'), 'single_reverse'),
            ('local_or_remote_mapping', (settings.NS.dct.creator, 'person'), 'local_or_remote_reverse'),
            ('local_or_remote_mapping', (settings.NS.dct.publisher, 'organization'), 'local_or_remote_reverse'),
                        ]



EXPE = ((1, 'Cerapcoop'),
        (2, 'EADSI Auvergne'))


class Newsletter(BaseNewsletter):
    expediteur = models.PositiveSmallIntegerField(u'Expéditeur',
                                                 choices=EXPE,
                                                 default=1)
    logo = ImageField(upload_to='logos/', null=True, blank=True)
    attachments = generic.GenericRelation('coop_local.Attachment')
    external_links = generic.GenericRelation('coop_local.Link')

    try:
        cerapcoop = AgendaCategory.objects.get(slug='evenement-cerapcoop')
        formation = AgendaCategory.objects.get(slug='formation')
    except ObjectDoesNotExist:
        raise ImproperlyConfigured(u"Les catégories d'évenement Formation et/ou Evenement CERAPCOOP sont manquantes.")
    try:
        mobilisation = ArticleCategory.objects.get(slug='campagnes-citoyennes')
    except ObjectDoesNotExist:
        raise ImproperlyConfigured(u"La catégories d'article Campagnes citoyennes est manquante")

    @property
    def news_grants(self):
        ct = ContentType.objects.get(app_label='coop_local', model='grant')
        return ct.model_class().objects.filter(id__in=
            self.elements.filter(content_type=ct).values_list('object_id', flat=True))

    def news_noform(self):
        return self.articles.exclude(agenda_category=self.formation)

    def news_cerapcoop(self):
        res = self.news_noform().filter(agenda_category=self.cerapcoop)
        res = list(res)
        res.sort(key=lambda x: x.occurences.all()[0])
        return res

    def agenda_auvergne(self):
        res = self.news_noform().filter(agenda_zone__in=[1,2,3,4]).exclude(agenda_category=self.cerapcoop)
        res = list(res)
        res.sort(key=lambda x: x.occurences.all()[0])
        return res

    def agenda_ailleurs(self):
        res = self.news_noform().filter(agenda_zone__in=[5,6,7]).exclude(agenda_category=self.cerapcoop)
        res = list(res)
        res.sort(key=lambda x: x.occurences.all()[0])
        return res

    def news_actus(self):
        return self.news_noform().filter(agenda_category__isnull=True).exclude(category=self.mobilisation)

    def news_agenda_ead(self):
        res = self.news_noform().filter(agenda_category__isnull=False).exclude(agenda_category=self.mobilisation)
        res = list(res)
        res.sort(key=lambda x: x.occurences.all()[0])
        return res


    def news_mob(self):
        return self.news_noform().filter(agenda_category__isnull=True).filter(category=self.mobilisation)

    def news_coop(self):
        coopdict = get_model('coop_local', 'Tag').objects.get(slug=u"coopération-décentralisée").tagged_items()
        results = {}
        for key in coopdict.keys():
            temp_list = []
            for item in coopdict[key]:
                if item.newsletter == self:
                    temp_list.append(item)
            if len(temp_list) > 0:
                results[key] = temp_list
        return results

    def news_formations(self):
        res = self.articles.filter(agenda_category=self.formation)
        res = list(res)
        res.sort(key=lambda x: x.occurences.all()[0].start_time)
        return res

    def news_outils(self):
        outils = get_model('coop_local', 'ResourceCategory').objects.get(slug='outil-methodologique')
        return self.resources.filter(category__in=[outils]) #filter !

    def news_ressources(self):
        outils = get_model('coop_local', 'ResourceCategory').objects.get(slug='outil-methodologique')
        return self.resources.exclude(category__in=[outils]) #exclude !

    def news_publications(self):
        films = get_model('coop_local', 'ResourceCategory').objects.get(slug='film')
        docu = get_model('coop_local', 'ResourceCategory').objects.get(slug='film-documentaire')
        video = get_model('coop_local', 'ResourceCategory').objects.get(slug='video-en-ligne')
        sites_web = get_model('coop_local', 'ResourceCategory').objects.get(slug='site-internet')
        return self.news_ressources().exclude(category__in=[films, sites_web, docu, video])

    def news_films(self):
        films = get_model('coop_local', 'ResourceCategory').objects.get(slug='film')
        docu = get_model('coop_local', 'ResourceCategory').objects.get(slug='film-documentaire')
        video = get_model('coop_local', 'ResourceCategory').objects.get(slug='video-en-ligne')
        return self.news_ressources().filter(category__in=[films,docu,video])

    def news_web(self):
        sites_web = get_model('coop_local', 'ResourceCategory').objects.get(slug='site-internet')
        return self.news_ressources().filter(category__in=[sites_web])

    def news_emploi(self):
        try:
            emploi = get_model('coop_local', 'AnnonceCategory').objects.get(slug='emploi')
            stage = get_model('coop_local', 'AnnonceCategory').objects.get(slug='stage')
            return self.news_annonces.all().filter(active=True, annonce_category__in=[emploi, stage])
        except ObjectDoesNotExist:
            raise ImproperlyConfigured(u"Les catégories d'annonces Emploi et/ou Stage sont manquantes.")
          

