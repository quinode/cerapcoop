# -*- coding:utf-8 -*-

from django.forms import ModelForm
from coop_local.models import Annonce
from django import forms
from datetimewidget.widgets import DateTimeWidget


class MLForm(forms.Form):
    error_css_class = 'error'
    required_css_class = 'required'
    email = forms.EmailField(label='email', widget=forms.TextInput(attrs={'placeholder': u'indiquez votre e-mail'}))
    first_name = forms.CharField(max_length=100, label='Prénom', required=True, 
                                widget=forms.TextInput(attrs={'placeholder': u'votre prénom'}))
    name = forms.CharField(max_length=100, label='Nom', required=True,
                            widget=forms.TextInput(attrs={'placeholder': u'votre nom'}))
    structure = forms.CharField(max_length=100, label='Structure', required=False,
                            widget=forms.TextInput(attrs={'placeholder': u'votre structure (optionnel)'}))

    adresse = forms.CharField(max_length=100, label='Adresse', required=False,
                            widget=forms.TextInput(attrs={'placeholder': u'adresse'}))
    code_postal = forms.CharField(max_length=5, label='Code postal', required=False,
                            widget=forms.TextInput(attrs={'placeholder': u'code postal'}))
    ville = forms.CharField(max_length=100, label='Commune', required=False,
                            widget=forms.TextInput(attrs={'placeholder': u'ville'}))



class AnnonceForm(ModelForm):
    class Meta:
        model = Annonce
        fields = ['label', 'description','annonce_category','web','expiration',
        'remote_person_label', 'tel', 'email', 'remote_organization_label']
        labels = {
            'remote_organization_label': 'Votre structure',
            'remote_person_label': 'Nom du contact'
        }
        dt_options = {'pickerPosition': 'top-right', 'weekStart' : 1 , 'minuteStep': 15 , 
        'format': 'dd/mm/yyyy', 'minView': 1}

        widgets = {
                #Use localization
                'expiration': DateTimeWidget(options= dt_options, attrs={'id':"expiration"}, usel10n = True)
            }
        # exclude = ['active','sites','slug','organization','remote_organization_uri',
        # 'tags','person','remote_person_uri', 'maj']
        # widgets = autocomplete_light.get_widgets_dict(Produit, Fournisseur)


class ReponseAnnonce(forms.Form):
    sender = forms.EmailField(label='Votre e-mail')
    message = forms.CharField(widget=forms.Textarea, label='Votre message')


class PostAgenda(forms.Form):
    dt_options = {'pickerPosition': 'top-right', 'weekStart' : 1 , 'minuteStep': 15, 
        'format': 'yyyy-mm-dd hh:ii', 'minView': 0}
    author = forms.CharField(label='Nom et prénom')
    org = forms.CharField(label='Organisme')
    titre = forms.CharField(label=u"Titre de l'événement")
    description = forms.CharField(widget=forms.Textarea, label=u"Description de l'événement")
    logo = forms.ImageField(required=False, label=u"Logo / Illustration")
    link = forms.URLField(label='Lien web', required=False)
    desc_link = forms.CharField(label='Intitulé du lien', required=False)
    dt_start = forms.DateTimeField( label="Date/heure début",
                                    widget=DateTimeWidget(options=dt_options, 
                                                attrs={'id':"dt_start"}, usel10n = True) 
                                    )
    dt_end = forms.DateTimeField(   label="Date/heure fin",
                                    widget=DateTimeWidget(options= dt_options, 
                                            attrs={'id':"dt_end"}, usel10n = True)
                                    )

