# -*- coding:utf-8 -*-
from django import forms
from django.contrib import admin
from django.core.exceptions import ImproperlyConfigured
from coop.utils.autocomplete_admin import FkAutocompleteAdmin, NoLookupsFkAutocompleteAdmin
from chosen import widgets as chosenwidgets
from django.db.models.loading import get_model
from tinymce.widgets import AdminTinyMCE
from django.db.models import URLField
from coop.org.admin import URLFieldWidget
from coop.utils.fields import MethodsCheckboxSelectMultiple
from coop_local.models import Organization, Person, Discipline, Niveau, Project, \
    DocResource, Exchange, Role, Area
from coop_local.models.local_models import (Grant, GrantTarget, Annonce, AnnonceCategory,
    AgendaCategory)
from coop.org.admin import EngagementInline, RelationInline, ContactInline, RoleAdmin, org_subs, hasPrefMail
from coop.doc.admin import ResourceAdmin
from coop.project.admin import ProjectAdmin, AttachmentsInline
from coop.exchange.admin import ExchangeAdmin, ExchangeInline
from coop_geo.admin import LocatedInline, AreaInline
from coop.agenda.admin import GenericDateInline
from coop.link.admin import LinksInline
from coop.article.admin import CoopArticleAdmin
from sorl.thumbnail.admin import AdminImageMixin
from django.contrib.admin import SimpleListFilter
from django.conf import settings

# from coop.doc.admin import ISBNWidget

admin.site.register(GrantTarget)
admin.site.register(Discipline)
admin.site.register(Niveau)
# admin.site.register(Log)
# admin.site.register(Logged)
# admin.site.register(LogEventType)

admin.site.register(AnnonceCategory)
admin.site.register(AgendaCategory)

# class AreaInline(AreaInline):
#     verbose_name = u'Zone géographique'
#     verbose_name_plural = u'Zones géographiques'


class GrantAdminForm(forms.ModelForm):
    description = forms.CharField(widget=AdminTinyMCE(attrs={'cols': 80, 'rows': 60}), required=False)

    def __init__(self, *args, **kwargs):
        super(GrantAdminForm, self).__init__(*args, **kwargs)
        self.fields['sites'].help_text = None
        self.fields['target'].help_text = None

    class Meta:
        model = get_model('coop_local', 'Grant')
        widgets = {'sites': chosenwidgets.ChosenSelectMultiple(),
                    'target': chosenwidgets.ChosenSelectMultiple(),}


class AnnonceAdminForm(forms.ModelForm):
    description = forms.CharField(widget=AdminTinyMCE(attrs={'cols': 80, 'rows': 60}), required=False)

    def __init__(self, *args, **kwargs):
        super(AnnonceAdminForm, self).__init__(*args, **kwargs)
        self.fields['sites'].help_text = None
        self.fields['active'].label = 'Annonce validée'

    class Meta:
        model = get_model('coop_local', 'Annonce')
        widgets = {'sites': chosenwidgets.ChosenSelectMultiple(),}


class MyAttachmentsInline(AttachmentsInline):
    model = get_model('coop_local', 'Attachment')

class MyGenericDateInline(GenericDateInline, FkAutocompleteAdmin):
    related_search_fields = {'commune_fr': ('label',),
                             'pays': ('label',),
                              }

class AnnonceAdmin(NoLookupsFkAutocompleteAdmin):
    change_form_template = 'admintools_bootstrap/tabbed_change_form.html'
    form = AnnonceAdminForm
    list_filter = ['annonce_category', 'active']
    list_display_links = ['label', ]
    list_display = ('is_active','label', 'organization_display', 'annonce_category')
    fieldsets = (
        ('Description', {'fields': ('sites', ('active'), 'label', 'annonce_category', 'description',
                                    'organization', 'remote_organization_label', 'remote_organization_uri',
                                    'person', 'remote_person_label', 'remote_person_uri',
                                    'tel','email',
                                    'web',
                                    'tags',
                                    ('expiration', 'maj'),
                                    )
                         }),
    )
    related_search_fields = {'person': ('last_name', 'first_name',
                                        'email', 'structure', 'username'),
                             'organization': ('title', 'acronym', 'subtitle', 'description'),
                             }
    formfield_overrides = {
        URLField: {'widget': URLFieldWidget},
    }

    def get_form(self, request, obj=None, **kwargs):
        # just save obj reference for future processing in Inline
        request._obj_ = obj
        return super(AnnonceAdmin, self).get_form(request, obj, **kwargs)

admin.site.register(Annonce, AnnonceAdmin)


class GrantAdmin(NoLookupsFkAutocompleteAdmin, AdminImageMixin):
    change_form_template = 'admintools_bootstrap/tabbed_change_form.html'
    form = GrantAdminForm
    list_display = ('logo_list_display', 'label', 'organization_display')
    list_display_links = ('logo_list_display', 'label')
    list_filter = ('grant_type', )
    fieldsets = (
        ('Description', {'fields': ('sites', 'logo', 'label', 'grant_type', 'target', 'summary', 'description',
                                    'organization', 'remote_organization_label', 'remote_organization_uri',
                                    'person', 'remote_person_label', 'remote_person_uri',
                                    'web',
                                    'tags',
                                    ('expiration_date', 'maj'),
                                    )
                         }),
    )
    search_fields = ('title', 'description')
    related_search_fields = {'person': ('last_name', 'first_name',
                                        'email', 'structure', 'username'),
                             'organization': ('title', 'acronym', 'subtitle', 'description'),
                             }
    inlines = [AreaInline, MyAttachmentsInline]
    formfield_overrides = {
        URLField: {'widget': URLFieldWidget},
    }

    def get_form(self, request, obj=None, **kwargs):
        # just save obj reference for future processing in Inline
        request._obj_ = obj
        return super(GrantAdmin, self).get_form(request, obj, **kwargs)


    if "coop.mailing" in settings.INSTALLED_APPS:
        def get_actions(self, request):
            from coop.mailing.admin import newsletter_actions
            my_actions = newsletter_actions
            return dict(my_actions, **super(GrantAdmin, self).get_actions(request))



admin.site.register(Grant, GrantAdmin)


# class LoggedInline(GenericTabularInline, InlineAutocompleteAdmin):
#     verbose_name = _(u'Historique')
#     verbose_name_plural = _(u'Historique')
#     model = Logged
#     extra = 1


try:
    from coop.base_admin import *
except Exception, exp:
    raise ImproperlyConfigured("Unable to find coop/base_admin.py file", exp)

admin.site.unregister(Article)
admin.site.unregister(SitePrefs)
admin.site.unregister(Organization)
admin.site.unregister(Person)
admin.site.unregister(Project)
admin.site.unregister(DocResource)
admin.site.unregister(Exchange)
admin.site.unregister(Role)
admin.site.unregister(Newsletter)


class MyArticleAdminForm(forms.ModelForm):

    content = forms.CharField(widget=AdminTinyMCE(attrs={'cols': 80, 'rows': 60}), required=False)


    def __init__(self, *args, **kwargs):
        super(MyArticleAdminForm, self).__init__(*args, **kwargs)
        self.fields['sites'].help_text = None
        self.fields['zone'].help_text = None
        self.fields['agenda_commune'].help_text = None
        self.fields['zone'].queryset = Area.objects.filter(area_type_id__exact=5).order_by('label')
        self.fields['agenda_public'].help_text = None
        templates = settings.COOP_CMS_ARTICLE_TEMPLATES
        self.fields['template'].widget = forms.Select(choices=templates)
        
    class Meta:
        model = get_model('coop_local', 'Article')
        widgets = {'sites': chosenwidgets.ChosenSelectMultiple(),
                    'agenda_public': chosenwidgets.ChosenSelectMultiple(),}
    
    def clean_agenda_category(self):
        data = self.cleaned_data['agenda_category']
        if(self.data['agenda_category'] != '' and 
           self.data['coop_local-genericdate-content_type-object_id-0-start_time_0'] == ''):
            raise forms.ValidationError(u"Vous ne pouvez pas choisir une catégorie d'événement si aucune date n'a été définie pour cet événement.")
        return data


class ArticleisEvent(SimpleListFilter):
    title = 'Type'
    parameter_name = 'type'
    def lookups(self, request, model_admin):
        return (
            ('event', 'Evenement'),
            ('article', 'Article simple'),
        )
    def queryset(self, request, queryset):
        if self.value() == 'article':
            return queryset.filter(agenda_category__isnull=True)
        if self.value() == 'event':
            return queryset.exclude(agenda_category__isnull=True)


class MyArticleAdmin(AdminImageMixin, CoopArticleAdmin):
    form = MyArticleAdminForm
    ordering = ['-maj']

    list_filter = [ ArticleisEvent, 'category', 'agenda_category']


    list_display = ['logo_list_display', 'title','headline', 'category', 'agenda_category']
    list_editable = [ 'headline', 'category']
    list_display_links = ['title']

    fieldsets = (
        ('Edition', {'fields': ['title','sites',
                                ('publication', 'category'),
                                'logo', 'summary','content', 'tags',
                                'organization', 'remote_organization_label', 'remote_organization_uri',
                                'person', 'remote_person_label', 'remote_person_uri', 'maj' ]}),
        ('Options', {'fields': ('template', 'headline', 'display_dates',)}),

        ('Agenda', {'fields': ('agenda_category',
                              'agenda_zone', 'agenda_commune', 'zone',
                              'agenda_public'
          )}),

        )
    inlines = [GenericDateInline, AttachmentsInline, LinksInline]
    added_sf = {'agenda_commune': ('label', 'reference'),
                             'zone': ('label',),}
    related_search_fields = dict(added_sf.items() + CoopArticleAdmin.related_search_fields.items())




admin.site.register(Article, MyArticleAdmin)


class MySitePrefsAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(MySitePrefsAdminForm, self).__init__(*args, **kwargs)
        self.fields['cerapcoop_tags'].help_text = None

    class Meta:
        model = get_model('coop_local', 'SitePrefs')
        widgets = {'cerapcoop_tags': chosenwidgets.ChosenSelectMultiple(),}



class MySitePrefsAdmin(SitePrefsAdmin, FkAutocompleteAdmin):
    form = MySitePrefsAdminForm
    change_form_template = 'admintools_bootstrap/tabbed_change_form.html'
    fieldsets = (
        ('Cerapcoop', {'fields': ['ml_cerapcoop', 'main_organization', 'cerapcoop_thematique', 
                                    ('cerapcoop_intitule_1', 'cerapcoop_article_1'),
                                    ('cerapcoop_intitule_2', 'cerapcoop_article_2'),
                                    'cerapcoop_initiative',
                                    'cerapcoop_tags']}),
        (('EAD-SI Auvergne'), {'fields' : ['ml_eadsi', 'eadsi_encart_1', 'eadsi_encart_2', 'eadsi_encart_3']}),
        )
    related_search_fields = {'main_organization': ('title', 'subtitle', 'acronym'),
                             'eadsi_encart_1': ('title', 'summary',),
                             'eadsi_encart_2': ('title', 'summary'),
                             'eadsi_encart_3': ('title', 'summary'),
                             'cerapcoop_thematique': ('name',),
                             'cerapcoop_article_1': ('title', 'summary',),
                             'cerapcoop_article_2': ('title', 'summary',),
                             'cerapcoop_initiative': ('title', 'subtitle', 'acronym'),
                             }

admin.site.register(SitePrefs, MySitePrefsAdmin)


class MyRoleAdmin(RoleAdmin):
    list_display = ('label', 'category', 'cible_base_peda')
    list_editable = ('category', 'cible_base_peda')
    fieldsets = (('Définition', {'fields' : ('sites', 'label', 'category', 'cible_base_peda')}),)

admin.site.register(Role, MyRoleAdmin)


class MyEngagementInline(EngagementInline):
    fields = ('person', 'role', 'role_detail', 'org_admin', 'est_contact', 'engagement_display')



class OrgaVille(SimpleListFilter):
    title = 'Ville'
    parameter_name = 'ville'
    def lookups(self, request, model_admin):
        return (

            ('63000', u'Clermont'),
        )
    def queryset(self, request, queryset):
        if self.value():
            return queryset.exclude(pref_address=None).filter(pref_address__zipcode=self.value())
        else:
            return queryset



class OrgaDepartment(SimpleListFilter):
    title = 'Dept'
    parameter_name = 'dept'
    def lookups(self, request, model_admin):
        return (

            ('03', u'Allier'),
            ('15', u'Cantal'),
            ('43', u'Haute-Loire'),
            ('63', u'Puy-de-dôme'),
        )
    def queryset(self, request, queryset):
        if self.value():
            return queryset.exclude(pref_address=None).filter(pref_address__zipcode__startswith=self.value())
        else:
            return queryset

class MyOrganizationAdmin(OrganizationAdmin, FkAutocompleteAdmin):
    # list_display = ['logo_list_display', 'label',  org_subs, 'active']#'has_description',

    list_max_show_all = 1500
    list_filter = ['dimension', 'category', 'statut', OrgaDepartment, OrgaVille, hasPrefMail, 'active']
    fieldsets = (
        ('Description', {'fields': ('sites', 'logo', 'title', ('acronym', 'pref_label'),
                                  'subtitle', ('birth', 'active', 'maj'),
                                  'statut', 'description', 'category', 'tags', 'web', 'accueil')
                       }),
        ('Cerapcoop', {'fields': ('code_gestion', 'dimension', 'budget',
                                  'part_pvd', 'outils_peda', 'salaries', 'adherents')
                       }),

        ('Préférences', {
            #'classes': ('collapse',),
            'fields': ('pref_email', 'pref_phone', 'pref_address', 'notes',)
        })
    )
    inlines = [MyEngagementInline,
               ContactInline,
               #LoggedInline,
               ExchangeInline,
               RelationInline,
               LocatedInline,
               AreaInline,
               AttachmentsInline,
               ]

admin.site.register(Organization, MyOrganizationAdmin)


class MyPersonAdmin(PersonAdmin, FkAutocompleteAdmin):
    # from coop.person.admin import PersonAdminForm
    # form = PersonAdminForm
    fieldsets = (
        ('Identification', {'fields': ('civilite', 'first_name', 'last_name', 'pref_email',
                                       'category',
                                       'tags',
                                       'structure',
                                       'notes')
                            }),

        ('Adresse postale',{'fields': (('location', 'location_display'),)}),
    )

admin.site.register(Person, MyPersonAdmin)


class MyProjectAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MyProjectAdminForm, self).__init__(*args, **kwargs)
        self.fields['ead_niveaux'].help_text = None
        self.fields['ead_disciplines'].help_text = None
        self.fields['sites'].help_text = None
        self.fields['category'].help_text = None
        self.fields['cible'].queryset = Role.objects.filter(cible_base_peda=True)

    class Meta:
        model = get_model('coop_local', 'Project')
        widgets = {'ead_niveaux': chosenwidgets.ChosenSelectMultiple(),
                   'ead_disciplines': chosenwidgets.ChosenSelectMultiple(),
                   'sites': chosenwidgets.ChosenSelectMultiple(),
                   'category': chosenwidgets.ChosenSelectMultiple(),
                   'cible': chosenwidgets.ChosenSelectMultiple(),
                }


class MyProjectAdmin(ProjectAdmin):
    form = MyProjectAdminForm
    fieldsets = (('Description', {'fields': ('sites', 'title', 'status', 'start', 'end', 'description', 'organization', 
                         'category', 'tags', ('active','maj'))
                }),
                 ('Cerapcoop', {'fields': ('cible', 'zone', 'budget', 'notes') #niveaux
        }),
                 ('EAD', {'fields': ('ead_objectifs', 'ead_disciplines', 'ead_niveaux', 'ead_nb_eleves', 'ead_nb_profs')
        })
    )


admin.site.register(Project, MyProjectAdmin)


class MyResourceAdminForm(forms.ModelForm):
    description = forms.CharField(widget=AdminTinyMCE(attrs={'cols': 80, 'rows': 60}), required=False)

    def __init__(self, *args, **kwargs):
        super(MyResourceAdminForm, self).__init__(*args, **kwargs)
        self.fields['cible'].queryset = self.fields['cible'].queryset.filter(cible_base_peda=True)
        self.fields['cible'].help_text = None
        self.fields['category'].help_text = None
        self.fields['zone'].queryset = Area.objects.filter(area_type_id__exact=5).order_by('label')
        self.fields['niveaux'].help_text = None
        self.fields['sites'].help_text = None

    class Meta:
        model = get_model('coop_local', 'DocResource')
        widgets = {'niveaux': chosenwidgets.ChosenSelectMultiple(),
                    'category': chosenwidgets.ChosenSelectMultiple(),
                    'cible': chosenwidgets.ChosenSelectMultiple(),
                    # 'isbn': ISBNWidget(),
                    'sites': chosenwidgets.ChosenSelectMultiple(),
               }


class MyResourceAdmin(ResourceAdmin):
    form = MyResourceAdminForm
    fieldsets = (
        ('Description', {'fields': ('sites', 'logo', 'label', 'category', 'description',
                                    'organization', 'remote_organization_label', 'remote_organization_uri',
                                    'person', 'remote_person_label', 'remote_person_uri',
                                    'tags', 'cible', 'niveaux', 'maj')
                         }),
        ('Details', {'fields': ('notes', 'page_url', 'file_url', 'isbn', 'zone', 'price')
                     }),
    )
    inlines = [AttachmentsInline, LinksInline]


admin.site.register(DocResource, MyResourceAdmin)

from coop.exchange.admin import ExchangeAdmin
from coop.exchange.admin import ExchangeAdminForm


class MyExchangeAdminForm(ExchangeAdminForm):

    class Meta:
        model = get_model('coop_local', 'Exchange')
        widgets = {'sites': chosenwidgets.ChosenSelectMultiple(),
                   'cible': chosenwidgets.ChosenSelectMultiple()}


class MyExchangeAdmin(ExchangeAdmin):  # AdminImageMixin,
    form = MyExchangeAdminForm
    fieldsets = (('Description', {'fields': [('eway', 'etype'),
                                     'methods',
                                     'title',
                                     'description', 'tags',
                                    'organization', 'remote_organization_label', 'remote_organization_uri',
                                    'person', 'remote_person_label', 'remote_person_uri', 'maj']},),)
                        

admin.site.register(Exchange, MyExchangeAdmin)


from coop.mailing.admin import NewsletterAdmin, NewsletterAdminForm
from django.contrib.sites.models import get_current_site

class MyNewsletterAdmin(NewsletterAdmin):
    fieldsets = (
        ('Description', {
            'fields': [ 'expediteur',
                        'subject', 'template',
                       'logo', 'content',
                       # 'articles', 'events',
                       'lists'
                       ]
            }),
        )
    inlines = [LinksInline, AttachmentsInline]


    def queryset(self, request):
        current_site = get_current_site(request)
        qs = super(MyNewsletterAdmin, self).queryset(request)
        if current_site.domain in ('debian:8000', 'debian', 'www.cerapcoop.org'):
            return qs.filter(expediteur=1)
        elif current_site.domain in ('debian2:8000', 'debian2', 'www.eadsi-auvergne.fr'):
            return qs.filter(expediteur=2)

admin.site.register(Newsletter, MyNewsletterAdmin)




