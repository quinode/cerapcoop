# -*- coding:utf-8 -*-
from django.conf import settings
from django.shortcuts import render_to_response, get_object_or_404
from coop_local.models import (Organization, Article, TaggedItem, Area, 
    Grant, Engagement, OrganizationCategory, Project, Tag, GenericDate, GrantTarget, Annonce,
    AnnonceCategory, AgendaCategory, ContactMedium, Contact, MailingList, Person, Subscription,
    Location, Person)
from coop_cms.models import ArticleCategory
from coop_cms.models import Link
from coop_geo.models import Located
from django.template import RequestContext
from django.contrib.sites.models import get_current_site
from haystack.views import SearchView
from django.contrib.sites.models import Site
from coop.views import ModelSearchView as BaseModelSearchView
from datetime import datetime
from django.contrib.contenttypes.models import ContentType
from coop_local.forms import AnnonceForm, ReponseAnnonce, PostAgenda
import pytz
from django.core.urlresolvers import reverse

now_tz = datetime.now().replace(tzinfo=pytz.UTC)

# def org_list(request):
#     context = {}
#     context['org_list'] = Organization.objects.filter(active=True).order_by('statut')
#     return render_to_response('org/org_list.html',context,RequestContext(request))


def home(request):
    context = {}
    current_site = get_current_site(request)
    if current_site.domain in ('debian2:8000', 'debian2', 'www.eadsi-auvergne.fr'):
        context['a_la_une'] = Article.objects.filter(headline=True).order_by('-maj')
    elif current_site.domain in ('debian:8000', 'debian', 'www.cerapcoop.org'):
        context['actualites'] = Article.objects.filter(headline=True).order_by('-maj')[:3]    
    return render_to_response('home.html', context, RequestContext(request))

def plan(request):
    context = {}
    current_site = get_current_site(request)
    if current_site.domain in ('debian2:8000', 'debian2', 'www.eadsi-auvergne.fr'):
        context['plan'] = 'navigation_eadsi'
    elif current_site.domain in ('debian:8000', 'debian', 'www.cerapcoop.org'):
        context['plan'] = 'navigation_cerapcoop'    
    return render_to_response('plan.html', context, RequestContext(request))



def tag_detail(request, slug, param=None):

    context = {}
    tag = Tag.objects.get(slug=slug)
    items = tag.tagged_items()
    
    form_id = AgendaCategory.objects.get(slug='formation').id
    mobil_id = ArticleCategory.objects.get(slug='campagnes-citoyennes').id

    find_articles_id = [article.id for article in items['articles']]
    articles_id = find_articles_id

    formations = Article.objects.filter(agenda_category_id=form_id, id__in=articles_id)
    for item in formations: articles_id.remove(item.id)

    mobilisations = Article.objects.filter(id__in=articles_id,category_id=mobil_id)
    for item in mobilisations: articles_id.remove(item.id)

    agenda = Article.objects.exclude(agenda_category__isnull=True).filter(id__in=articles_id)
    for item in agenda: articles_id.remove(item.id)

    articles = [item for item in items['articles'] if item.id in articles_id]
    # les articles : evenements, mobilisations et formations ont été récupérés,
    # ensuite on nettoie les tagged_items
    types_to_exclude = [u'articles', u'Personnes', u'Persons', u'mailing lists', u'listes']

    for t in types_to_exclude:
        if t in items.keys():
            del items[t]

    context = { 'formations': sorted(formations, key=lambda x: x.maj, reverse=True), 
                'mobilisations':sorted(mobilisations, key=lambda x: x.maj, reverse=True), 
                'agenda':sorted(agenda, key=lambda x: x.maj, reverse=True), 
                'articles':sorted(articles, key=lambda x: x.maj, reverse=True),
                'tagged_items': items,
                'tag' : tag, 
                'param' : param}

    return render_to_response('tag/tag_detail_mod.html', context , RequestContext(request))



from coop_local.forms import MLForm

def inscription_mailing(request):
    context = {}
    if request.method == 'POST':
        form = MLForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            mail = ContactMedium.objects.get(label='Email')
            current_site = get_current_site(request)
            from preferences import preferences
            if current_site.domain in ('debian2:8000', 'debian2', 'www.eadsi-auvergne.fr'):
                liste = preferences.SitePrefs.ml_eadsi
            elif current_site.domain in ('debian:8000', 'debian', 'www.cerapcoop.org'):
                liste = preferences.SitePrefs.ml_cerapcoop
            contact = Contact.objects.filter(contact_medium=mail, content=data['email'])

            if contact.exists():
                if contact[0].content_object.subs.filter(mailing_list_id__in=[liste.id]).exists():
                    context['msg'] = u"Cette adresse e-mail est déjà abonnée à la newsletter."
                    context['mailing_form'] = MLForm(request.POST)
                    return render_to_response('ajax/mailing_form.html', context, RequestContext(request))
                else:
                    subscriber = contact.content_object
            else:
                if len(data['structure']) > 0: # c'est une structure
                    subscriber = Organization.objects.create(title=data['structure'])
                    if len(data['adresse']) > 0:
                        adresse = Location.objects.create(adr1=data['adresse'],
                                                        zipcode=data['code_postal'],
                                                        city=data['ville'])
                        Located.objects.create(location=adresse, content_object=subscriber)
                    person = Person.objects.create(last_name=data['name'],
                                    first_name=data['first_name'])
                    Engagement.objects.create(organization=subscriber,person=person)
                    Contact.objects.create(contact_medium=mail, 
                            content_object=person, 
                            content=data['email'])
                    subscriber.save() # in order to update org's pref_email
                    subscriber_url = reverse('admin:coop_local_organization_change', args=(subscriber.id,))

                else: #c'est une personne
                    subscriber = Person.objects.create(last_name=data['name'],
                                    first_name=data['first_name'])
                    Contact.objects.create(contact_medium=mail, 
                            content_object=subscriber, 
                            content=data['email'])
                    subscriber_url = reverse('admin:coop_local_person_change', args=(subscriber.id,))

            notif = u'Inscription a la newsletter %s du site\n\n' % (liste.name, current_site.domain )
            if len(data['structure']) > 0:
                notif += 'Structure : <a href="%s">%s</a> \n' % (subscriber_url, subscriber)

                #TODO

            Subscription.objects.create(mailing_list=liste, content_object=subscriber)
            from django.core.mail import mail_admins
            mail_admins(u'Inscription Newsletter', 
                        u'Inscription de : %s %s' , fail_silently=False)
            context['msg'] = "Inscription effectuée. Vous recevrez notre prochaine lettre d'information."
        else:
            context['mailing_form'] = MLForm(request.POST)
    return render_to_response('ajax/mailing_form.html', context, RequestContext(request))



def agenda_global(request, filter=None):
    from coop_local.models.local_models import ZONAGE_AGENDA
    all_events = Article.objects.exclude(agenda_category__isnull=True)
    orgas_ids = set(all_events.values_list('organization_id', flat=True))

    tags = set()
    for z in all_events:
        for t in z.tags.all():
            tags.add(t)

    context = {'filter': filter, 
                'years': range(2012, datetime.now().year)[::-1],
                'event_types': AgendaCategory.objects.all(),
                'zones': ZONAGE_AGENDA.CHOICES_DICT,
                'publics': GrantTarget.objects.all(),
                'orgas':  Organization.objects.filter(id__in=orgas_ids),
                'periode': 'now',
                'selcat': 'all',
                'orga': 'all',
                'public': 'all',
                'form': PostAgenda,
                'seltag': 'all',
                'tags' : tags
                }

    if request.method == 'POST':
        args = request.POST
    else:
        args = {'periode' : 'now'}

    if filter == 'Formation':
        args['selcat'] = AgendaCategory.objects.get(slug='formation').id

    if filter == 'Cerapcoop':
        args['orga'] = Organization.objects.get(slug="cerapcoop").id

    if filter == 'Auvergne':
        args['lieu'] = [1,2,3,4]

    if(len(args) > 1 or filter):
        periode = args['periode']

        if 'selcat' in args.keys() and args['selcat'] != 'all':
            cat = args['selcat']
            all_events = all_events.filter(agenda_category_id=cat)
            context['selcat'] = cat

        if 'orga' in args.keys() and args['orga'] != 'all':
            all_events = all_events.filter(organization_id=args['orga'])
            context['orga'] = str(args['orga'])
            periode = 'all'

        if 'public' in args.keys() and args['public'] != 'all':
            cible = GrantTarget.objects.get(id=args['public'])
            all_events = all_events.filter(agenda_public__in=[cible])
            context['public'] = args['public']

        if periode == 'now': # si pas de periode sélectionnée , on est sur les events à venir
            selected_events = set()
            for event in all_events:
                for occurence in event.occurences.all():
                    if occurence.end_time > now_tz:
                        selected_events.add(event.id)
            all_events = all_events.filter(id__in=selected_events)
            context['periode'] = 'now'     

        elif periode == 'all':
            context['periode'] = 'all' 

        elif periode != 'now':
            selected_events = set()
            selected_year = int(periode)
            for event in all_events:
                for occurence in event.occurences.all():
                    if occurence.end_time.year == selected_year:
                        selected_events.add(event.id)
            all_events = all_events.filter(id__in=selected_events)
            context['periode'] = str(selected_year)

        if 'lieu' in args.keys() and args['lieu'] != 'all':
            if type(args['lieu']) == list:
                all_events = all_events.filter(agenda_zone__in=args['lieu'])
            else:
                all_events = all_events.filter(agenda_zone=args['lieu'])
            context['lieu'] = args['lieu']

        if 'tag' in args.keys() and args['tag'] != 'all':
            t = Tag.objects.get(id=args['tag'])
            cta = ContentType.objects.get(model='article')
            events_id = all_events.values_list('id',flat=True)
            tagged = TaggedItem.objects.filter( tag=t, 
                                                content_type=cta, 
                                                object_id__in=events_id)
            all_events = all_events.filter(id__in=tagged.values_list('object_id', flat=True))
            context['seltag'] = args['tag']

    else : #on est en mode GET donc avec periode= now par défaut
        selected_events = set()
        for event in all_events:
            for occurence in event.occurences.all():
                if occurence.end_time > now_tz:
                    selected_events.add(event.id)
        all_events = all_events.filter(id__in=selected_events)

    # context['tags'] = tags

    # done = []

    # trier les evenements par leur premiere date connue
    all_events = list(all_events)
    all_events.sort(key=lambda x: x.occurences.all()[0])

    context['events'] = all_events

    if(request.method == 'POST'):
        results = len(all_events)
        if results == 0:
            if request.POST['periode'] == 'all':
                context['result_info'] = u'Aucun résultat pour ces paramètres de recherche.'
            else :
                context['result_info'] = u'Aucun résultat pour ces paramètres de recherche.<br>Vous\
                pouvez également <a href="#" id="elargir">élargir la recherche à toutes les périodes</a>.'
        elif results == 1:
            context['result_info'] = u'Un seul résultat pour cette recherche.'
        elif results > 1:
            context['result_info'] = u'%d résultats pour cette recherche.' % (results)

    return render_to_response('agenda/agenda.html', context, RequestContext(request))


def add_event(request):
    context = {'form': PostAgenda
                }
    if(request.method == 'POST'):
        form = PostAgenda(request.POST, request.FILES)
        if form.is_valid():
            article = Article(  title=request.POST['titre'],
                                content=request.POST['description'],
                                publication=0,
                                remote_person_label=request.POST['author'],
                                remote_organization_label=request.POST['org'],
                                )
            if request.FILES['logo']:
                article.logo=request.FILES['logo']
            article.save()
            new_event = GenericDate( start_time=datetime.strptime(request.POST['dt_start_0'], '%d/%m/%Y %H:%M:%S'),
                                end_time=datetime.strptime(request.POST['dt_end_0'], '%d/%m/%Y %H:%M:%S'),
                                content_object=article)
            new_event.save()
            if request.POST['link']:
                ct = ContentType.objects.get(app_label='coop_local', model='article')
                link = request.POST['link']
                desc_link = request.POST['desc_link']
                real_desc = (link, desc_link)[desc_link != '']
                l = Link()
                l.object_uri=link
                l.object_label=real_desc
                l.content_type=ct
                l.object_id=article.id
                l.save()
            from django.core import urlresolvers
            change_url = urlresolvers.reverse('admin:coop_local_article_change', args=(article.id,))
            from django.core.mail import mail_admins
            email_text = u'''Un nouvel événement a été posté sur le site : 
Sujet : "''' + unicode(article.title) + u'''" 
Texte : 
    ''' + unicode(article.content) + u'''
                
Pour le valider, suivez le lien suivant : 
http://'''+ settings.DEFAULT_URI_DOMAIN + change_url 
            mail_admins(u'Nouvel événement posté sur le site', email_text , fail_silently=False)
            context['msg'] = u'Événement "'+ unicode(article.title) + u'" proposé pour validation'
        else:
            context['form'] = PostAgenda(request.POST)
            context['error'] = u'Merci de corriger le formulaire'  
    return render_to_response('agenda/add_event.html', context, RequestContext(request))


def annonce_detail(request, slug):
    annonce = get_object_or_404(Annonce, slug=slug)
    context = {'item': annonce, 'form': ReponseAnnonce}
    if request.method == "POST":
        form = ReponseAnnonce(request.POST)
        if form.is_valid():
            from django.core.mail import send_mail
            send_mail(subject=u"Réponse à votre annonce sur le site Cerapcoop",
                recipient_list=[annonce.email],
                from_email=request.POST['sender'],
                message=request.POST['message'])
            context['msg'] = u"Votre message a été envoyé à l'auteur de l'annonce"
        else:
            context['form'] = ReponseAnnonce(request.POST)
            context['error'] = u'Merci de corriger le formulaire'
    return render_to_response('annonces/annonce_detail.html', context, RequestContext(request))


def annonces_list(request):
    context = {'form': AnnonceForm}
    context['annonces'] = Annonce.objects.filter(active=True)
    context['cat_list'] = AnnonceCategory.objects.all()
    if request.method == "POST":
        form = AnnonceForm(request.POST)
        if form.is_valid():
            annonce = form.save()
            context['msg'] = u'Annonce "'+ unicode(annonce.label) + u'" proposée pour validation'
        else:
            # import pdb; pdb.set_trace()
            context['form'] = AnnonceForm(request.POST)
            context['error'] = u'Merci de corriger le formulaire'
    return render_to_response('annonces/annonces_list.html', context, RequestContext(request))


def annonces_category(request, slug):
    cat = get_object_or_404(AnnonceCategory, slug=slug)
    context = {'annonce_category': cat, 'form': AnnonceForm}
    context['annonces'] = Annonce.objects.filter(annonce_category=cat, active=True)
    if request.method == "POST":
        form = AnnonceForm(request.POST)
        if form.is_valid():
            annonce = form.save()
            context['msg'] = u'Annonce "'+ unicode(annonce.label) + u'" proposée pour validation'
        else:
            # import pdb; pdb.set_trace()
            context['form'] = AnnonceForm(request.POST)
            context['error'] = u'Merci de corriger le formulaire'
    return render_to_response('annonces/annonces_list.html', context, RequestContext(request))


def grant_detail(request, slug):
    context = {'item': get_object_or_404(Grant, slug=slug)}
    return render_to_response('ressources/grant_detail.html', context, RequestContext(request))


def grants_list(request):
    context = {}
    from coop_local.models.local_models import GRANTS_TYPE
    from coop_geo.models import AreaType
    zonetype = AreaType.objects.get(txt_idx='ZONE')
    context['gtypes'] = GRANTS_TYPE
    context['gtargets'] = GrantTarget.objects.all()
    context['gzones'] = Area.objects.filter(area_type__id=zonetype.id)

    tags = set()
    for item in TaggedItem.objects.filter(content_type=ContentType.objects.get(model='grant')):
        if get_current_site(request) in item.content_object.sites.all():
            tags.add(item.tag)
    context['tags'] = tags

    grants = Grant.objects.all()
    
    for param in ('seltype', 'selcible', 'seltag', 'selzone'):
        context[param] = 'all'
        if(request.method == 'POST' and request.POST.get(param) != 'all'):
            context[param] = request.POST.get(param)

    if(context['seltype'] != 'all'):
        grants = grants.filter(grant_type=context['seltype'])

    if(context['selcible'] != 'all'):
        grants = grants.filter(target__in=GrantTarget.objects.filter(id=context['selcible']))

    if(context['seltag'] != 'all'):
        t = Tag.objects.filter(id=context['seltag'])
        if t.exists():
            grants = grants.filter(tags=t[0])

    if(context['selzone'] != 'all'):
        valid = []
        for grant in grants:
            if grant.in_zone(int(context['selzone'])):
                valid.append(grant.id)
        grants = grants.filter(id__in=valid)


    context['financements'] = grants.order_by('-expiration_date')
    results = grants.count()
    if results == 0:
        context['result_info'] = u'Aucun résultat pour ces paramètres de recherche.'
    elif results == 1:
        context['result_info'] = u'Un seul résultat pour cette recherche.'
    elif results > 1:
        context['result_info'] = u'%d résultats pour cette recherche.' % (results)
    qs = Link.objects.filter(url=request.path)
    if qs.exists():
        context['link'] = qs[0]
    return render_to_response('ressources/grants_list.html', context, RequestContext(request))



def contacts(request):
    cat = OrganizationCategory.objects.filter(slug='pilotage-ead-auvergne')
    pilotes = Organization.objects.filter(category__in=cat)
    eng = Engagement.objects.filter(organization__in=pilotes, est_contact=True)
    context = {'engagements': eng}
    return render_to_response('org/contacts.html', context, RequestContext(request))


# def discipline_detail(request, slug):
#     d = get_object_or_404(Discipline, slug=slug)
#     context = { 'discipline': d,
#                 'projets': Project.objects.filter(ead_disciplines__in=d)}
#     return render_to_response('project/discipline.html', context, RequestContext(request))


class ModelSearchView(BaseModelSearchView):
    def __name__(self):
        return "ModelSearchView"

    def extra_context(self):
        extra = super(ModelSearchView, self).extra_context()

        # the call to .__dict__['query'].get_results() is a work around due 
        # the issue #575

        results = self.results
        search = self.form.search()
        if 'whoosh' in settings.HAYSTACK_CONNECTIONS['default']['ENGINE']:

            res = results.models(Project).__dict__['query'].get_results()
            if res == []:
                extra['project'] = search.models(Project).__dict__['query'].get_results()
            else:
                extra['project'] = res

            res = results.models(Grant).__dict__['query'].get_results()
            if res == []:
                extra['grant'] = search.models(Grant).__dict__['query'].get_results()
            else:
                extra['grant'] = res
        else:
            if results.models(Project).count() == 0:
                extra['project'] = search().models(Project)
            else:
                extra['project'] = results.models(Project)

            if results.models(Grant).count() == 0:
                extra['grant'] = search().models(Grant)
            else:
                extra['grant'] = results.models(Grant)
        return extra


class SiteModelSearchView(ModelSearchView):
    def __name__(self):
            return "SiteModelSearchView"

    def __init__(self, *args, **kwargs):
        # Needed to switch out the default form class.
        if not 'site' in kwargs:
            self.site = Site.objects.get_current()
        else:
            site = kwargs.pop('site')
            if site:
                self.site = site
            else:
                self.site = Site.objects.get_current()
        super(SiteModelSearchView, self).__init__(*args, **kwargs)

    def build_form(self, form_kwargs=None):
        form = super(SiteModelSearchView, self).build_form(form_kwargs)
        if hasattr(form, 'set_site'):
            form.set_site(self.site)
        return form

    def extra_context(self):
        extra = super(SiteModelSearchView, self).extra_context()
        extra['site'] = self.site
        return extra
