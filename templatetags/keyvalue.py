from django import template
register = template.Library()

@register.filter
def keyvalue(dict, key):
    inner_list = dict.get(key,'')
    newlist = sorted(inner_list, key=lambda x: x.maj, reverse=True) #attention special tagged_items
    return newlist
