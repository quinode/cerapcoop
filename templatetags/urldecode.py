from django import template
register = template.Library()
from urllib import unquote

@register.filter
def urldecode(value):
    return unquote(value)