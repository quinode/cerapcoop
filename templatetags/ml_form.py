from coop_local.forms import MLForm
from django import template
register = template.Library()

@register.assignment_tag
def ml_form():
    return MLForm()